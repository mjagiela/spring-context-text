package pl.jagm.springcontexttest;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextHierarchy({
        @ContextConfiguration(classes = AppTest.TestConfig.class),
        @ContextConfiguration("classpath:spring-context.xml")
})
@Slf4j
public class AppTest {

    @Test
    public void testContext() {
        assertTrue(true);
    }

    @Configuration
    @Slf4j
    public static class TestConfig {

        @Bean
        @Primary
        Provider credentialsProvider() {
            log.info("Creating new provider");
            return () -> log.info("Create provider");
        }
    }
}


