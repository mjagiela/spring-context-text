package pl.jagm.springcontexttest;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public class CredentialsUser {

    @Autowired
    private Provider credentialsProvider;

    public CredentialsUser() {
    }

    @PostConstruct
    public void createCredentials() {
        credentialsProvider.create();
    }
}
